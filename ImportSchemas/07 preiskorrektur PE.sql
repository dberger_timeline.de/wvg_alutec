﻿update preise, art
set  preise.preis = preise.preis / (select alutec_dueb_preiseinh from persart where art_artnr = preise.artnr and persart.pers_typ = preise.pers_typ and persart.pers_nr = preise.pers_nr and persart.typ = 0) * art.preiseinh,
	preise.me_nr = (select nr from me where klasse = 101 and faktor_klassenr = (select alutec_dueb_preiseinh from persart where art_artnr = preise.artnr and persart.pers_typ = preise.pers_typ and persart.pers_nr = preise.pers_nr and persart.typ = 0))
where preise.artnr = art.artnr
and preise.pers_typ = 2
and preise.typ = 30
and (select alutec_dueb_preiseinh from persart where art_artnr = preise.artnr and persart.pers_typ = preise.pers_typ and persart.pers_nr = preise.pers_nr and persart.typ = 0) is not null
//and preise.artnr = '20002'
;


update preise, art, mstaff
set  mstaff.preis = mstaff.preis / (select alutec_dueb_preiseinh from persart where art_artnr = preise.artnr and persart.pers_typ = preise.pers_typ and persart.pers_nr = preise.pers_nr and persart.typ = 0) * art.preiseinh
where preise.artnr = art.artnr
and preise.pers_typ = 2
and preise.typ = 30
and (select alutec_dueb_preiseinh from persart where art_artnr = preise.artnr and persart.pers_typ = preise.pers_typ and persart.pers_nr = preise.pers_nr and persart.typ = 0) is not null
and mstaff.preise_nr = preise.nr
and mstaff.typ = 10
;