
--Bearbeiterpreise...alle einkaufspreise zu den B0 Artikeln auf Bearbeiterpreis setzen in persart und preise!
update art, arbgkat, persart
set persart.typ = 10, persart.arbgkat_nr = arbgkat.id
where left(art.artnr,2) = 'B0'
and art.ind_str10_02 = arbgkat.id
and art.artnr = persart.art_artnr
and persart.pers_typ = 2
and persart.typ = 0 ;

update art, arbgkat, preise
set preise.typ = 40, preise.arbgkat_nr = arbgkat.id
where left(art.artnr,2) = 'B0'
and art.ind_str10_02 = arbgkat.id
and art.artnr = preise.artnr
and preise.pers_typ = 2
and preise.typ = 30 ;

UPDATE art SET artikeltyp_ek = 0, artikeltyp_prod = 1 WHERE left(art.artnr,2) = 'B0';