﻿delete from persart where not exists (select 1 from art where artnr = art_artnr);

  INSERT INTO "preise"  
         ( "nr",   
           "typ",   
           "artnr",   
           "pers_typ",   
           "pers_nr",   
           "arbgkat_nr",   
           "preis",   
           "rabatt",   
           "rabatt_typ",   
           "me_nr",   
           "preis_me",
			"gueltig_ab")
select 	(select max(nr) from preise) + number(),
			30,
			art_artnr,
			pers_typ,
			pers_nr,
			arbgkat_nr,
			0,
			0,
			0,
			(select me_nr from art where artnr = art_artnr),
			0,
			today()
from persart
where pers_typ = 1
and not exists 
		(select 1 
			from preise 
			where preise.typ = 30 
			and preise.artnr = persart.art_artnr 
			and preise.pers_typ = persart.pers_typ 
			and preise.pers_nr = persart.pers_nr
			and preise.arbgkat_nr = persart.arbgkat_nr) ;