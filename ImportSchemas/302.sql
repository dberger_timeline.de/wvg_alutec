
--legierungskontrakte / MTZ
  INSERT INTO "para2"  
         ( "grp",   
           "id",   
           "wert" )  
	ON EXISTING SKIP
  VALUES ( 'MTZUmstellGrp',   
           '-',   
           'unbekannt' )  ;


DELETE FROM "belpos_detail" WHERE "mtz_nr" IS NOT NULL;
DELETE FROM "zsl_persart" WHERE "mtz_nr" IS NOT NULL;
DELETE FROM "mtz" WHERE typ = 10;

INSERT INTO "mtz"  
         ( "nr",   
           "typ",   
           "bez",   
           "werkstoff",   
           "basis",   
           "abbrand_handling",   
           "berech_art",   
           "buchungszeitpkt",   
           "umstellgrp",   
           "waehrg_kz",   
           "limitierg" )  
SELECT COALESCE((select max(nr) from mtz),0) + number(),		//	( "nr",   
		10,				//	  "typ", 
		art.artnr + ' ' + CAST(pers_nr AS CHAR),		  
		art.werkstoff,		//	  "werkstoff",   
		art.ind_dbl_02,	//	  "basis",   
		art.ind_dbl_03,	//	  "abbrand_handling",   
		'NB',			//	  "berech_art",   
		'A',			//	  "buchungszeitpkt",   
		'-',			//	  "umstellgrp",   
		1,				//	  "waehrg_kz",   
		'Z'	//	  "limitierg" )  
from art, persart
where coalesce(art.ind_dbl_01,0) > 0
and art.werkstoff <> '-'
and pers_typ = 1
and persart.art_artnr = art.artnr;

  INSERT INTO "zsl_persart"  
         ( "lfdnr",   
           "typ",   
           "artnr",   
           "pers_typ",   
           "pers_nr",   
           "zslid",   
           "bez1",   
           "bez2",   
           "berech_art",   
           "mtz_nr",   
           "gewicht" )  
SELECT 	COALESCE((select max(lfdnr) from zsl_persart),0) + number(),			//( "lfdnr",   
			20,	//  "typ",   
			persart.art_artnr,//  "artnr",   
			persart.pers_typ,//  "pers_typ",   
			persart.pers_nr,//  "pers_nr",   
			'MTZ',	//  "zslid",   
			zsl.bez1, //  "bez1",   
			zsl.bez2, //  "bez2",   
			42,		//  "berech_art",   
			(select nr from mtz where bez = art.artnr + ' ' + CAST(persart.pers_nr AS CHAR)), //  "mtz_nr",   
			art.ind_dbl_01		//  "gewicht" )  
from art, persart, zsl
where coalesce(art.ind_dbl_01,0) > 0
and art.werkstoff <> '-'
and pers_typ = 1
and persart.art_artnr = art.artnr
and zsl.id = 'MTZ';