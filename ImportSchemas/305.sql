

delete from belpos where bel_typ = 15 and pos_typ <> 'A';

  INSERT INTO "bel_adr"  
         ( "lfd_nr",   
           "bel_typ",   
           "bel_nr",   
           "name1",   
           "zeile1",   
           "plz",   
           "ort",   
           "land",   
           "pers_typ",   
           "pers_nr",   
           "laender_kz",   
           "adr_typ" )  
SELECT     COALESCE((select max(lfd_nr) from bel_adr),0) + number(),
				"bel"."typ",   
           "bel"."nr",   
           "adr1name1",   
           "adr1zeile1",   
           "adr1plz",   
           "adr1ort",   
           "adr1land",   
           "pers_typ",   
           "pers_nr",   
           "laender_kz",   
           10   
FROM bel 
WHERE typ = 15
AND NOT EXISTS (SELECT 1 FROM bel_adr WHERE bel_typ = bel.typ AND bel_nr = bel.nr);
 