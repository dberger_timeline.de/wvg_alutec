﻿
update art, persart
set persart.zeich_nr = art.ind_text
where art.artnr = persart.art_artnr
and persart.pers_typ = 1
and persart.typ = 0
and COALESCE(art.ind_text,'') > '';

update art set art.zeich_nr = art.ind_text where coalesce(art.zeich_nr,'') = '';

update art set art.ind_text = NULL;