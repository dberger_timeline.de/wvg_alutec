INSERT INTO "inventur"  
	( "id", "datum1", "status", "soll_wert_typ", "ist_wert_typ", "typ")  
ON EXISTING UPDATE
VALUES
	('INVSAGE', today(), 10, 'B', 'B', 1 );

INSERT INTO "inv_zaehlg" ( "inventur_id", "nr", "typ", "mitarb_id", "timestmp", "status", "lager_nr", "lagerprot_nr_bei_eroeffng", "sperre", "mehrfachzaehlg", "hybrid", "gebinde_jn" )  
values ('INVSAGE', 1, 10, 'AG', today(), 0, 1, (select max(buchungsnr) from lagerprot), 0, 1, 'INV_ZAEHLG^INVSAGE^1', 0);

INSERT INTO "inv_zaehlg" ( "inventur_id", "nr", "typ", "mitarb_id", "timestmp", "status", "lager_nr", "lagerprot_nr_bei_eroeffng", "sperre", "mehrfachzaehlg", "hybrid", "gebinde_jn" )  
values ('INVSAGE', 2, 10, 'AG', today(), 0, 9000, (select max(buchungsnr) from lagerprot), 0, 1, 'INV_ZAEHLG^INVSAGE^2', 0);

INSERT INTO "inv_zaehlg" ( "inventur_id", "nr", "typ", "mitarb_id", "timestmp", "status", "lager_nr", "lagerprot_nr_bei_eroeffng", "sperre", "mehrfachzaehlg", "hybrid", "gebinde_jn" )  
with test (inventur_id, typ, mitarb_id, timestmp, status, lager_nr, lagerprot_nr_bei_eroeffng, sperre, mehrfachzaehlg, hybrid, gebinde_jn) 
AS (SELECT  distinct 'INVSAGE', 10, 'AG', today(), 0, "lagernr", (select max(buchungsnr) from lagerprot), 0, 1, 'INV_ZAEHLG^INVSAGE^', 0
    FROM "dueb_bestand"  
WHERE "dueb_bestand"."istfremdlager" = -1
//and COALESCE(user_lageruebernahme,0) = -1
AND EXISTS (SELECT 1 FROM lager WHERE nr = CAST("dueb_bestand"."lagernr" AS CHAR))) 
select test.inventur_id, (select max(nr) from inv_zaehlg where inventur_id = 'INVSAGE') + number(), test.typ, test.mitarb_id, test.timestmp, test.status, test.lager_nr, test.lagerprot_nr_bei_eroeffng, test.sperre, test.mehrfachzaehlg, test.hybrid + CAST((select max(nr) from inv_zaehlg where inventur_id = 'INVSAGE') + number() as char), test.gebinde_jn
from test;

INSERT INTO "inv_bestand" ( "inventur_id", "nr", "typ", "inv_zaehlg_nr", "status", "lagernr", "artnr", "sollmenge", "zaehlmenge" )  
SELECT inv_zaehlg.inventur_id, number(), 10, inv_zaehlg.nr, 10, inv_zaehlg.lager_nr, art."artnr", 0, 0
FROM inv_zaehlg, art
WHERE inv_zaehlg.inventur_id = 'INVSAGE' 
AND COALESCE(art.lagerjn,0) = 1
AND COALESCE(art.sperrkz,0) = 0
ORDER BY inv_zaehlg.nr, art.artnr;

INSERT INTO "inv_zaehlg_pos" ( "inventur_id", "nr", "inv_zaehlg_nr", "typ", "lagernr", "artnr", "menge", "status" )  
  SELECT 'INVSAGE',
			number(),
			(SELECT nr FROM inv_zaehlg WHERE "inventur_id" = 'INVSAGE' AND lager_nr = (if dueb_bestand.istsperrlager = -1 then 9000 else if dueb_bestand.istfremdlager = -1 then dueb_bestand.lagernr else 1 endif endif)), 
			10,
		   (if dueb_bestand.istsperrlager = -1 then 9000 else if dueb_bestand.istfremdlager = -1 then dueb_bestand.lagernr else 1 endif endif),
         "dueb_bestand"."artnr",   
         "dueb_bestand"."bestand",
			1
    FROM "dueb_bestand"  
WHERE EXISTS (SELECT 1 FROM art WHERE artnr = "dueb_bestand"."artnr" AND COALESCE(art.lagerjn,0) = 1)
//AND "dueb_bestand"."user_lageruebernahme" = -1
;