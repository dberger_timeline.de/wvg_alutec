SELECT [vwStammdatenAdressen].[Adresse]
      ,[vwStammdatenAdressen].[Mandant]
  ,[KHKKontokorrent].[Kto] as tl_persnr
  ,[KHKKontokorrent].[Preisliste]
      ,[KHKPreislisten].[Bezeichnung]
      ,[KHKPreislisten].[IstAktion]
      ,[KHKPreislisten].[Basisliste]
      ,[KHKPreislisten].[Aktionsliste]
      ,[KHKPreislisten].[GueltigVon]
      ,[KHKPreislisten].[GueltigBis]
      ,[KHKPreislisten].[IstBruttopreis]
      ,[KHKPreislisten].[IstRabattfaehig]
      ,[KHKPreislisten].[Memo]
      ,[KHKPreislisten].[WKz]
      ,[KHKPreislisten].[DezimalstellenPreis]
      ,[KHKPreislisten].[Listentyp]
      ,[KHKPreislisten].[Aktiv]
      ,[KHKPreislistenArtikel].[Artikelnummer] 
     ,[KHKPreislistenArtikel].[AuspraegungID] 
     ,[KHKPreislistenArtikel].[AbMenge] 
     ,[KHKPreislistenArtikel].[Einzelpreis] 
 
  FROM [vwStammdatenAdressen] LEFT OUTER JOIN [KHKKontokorrent] ON [KHKKontokorrent].[Adresse] = [vwStammdatenAdressen].[Adresse] AND [KHKKontokorrent].[Mandant] = [vwStammdatenAdressen].[Mandant], [OLWVG].[dbo].[KHKPreislisten], [KHKPreislistenArtikel]
  WHERE [KHKPreislisten].ID = [KHKKontokorrent].[Preisliste]
  AND [KHKPreislisten].Mandant = [KHKKontokorrent].Mandant
  AND [KHKPreislistenArtikel].Mandant = [KHKKontokorrent].Mandant
  AND (([KHKPreislistenArtikel].ListeID = [KHKPreislisten].[Aktionsliste] AND [KHKPreislisten].[Aktionsliste] > 0)
OR
([KHKPreislistenArtikel].ListeID = [KHKPreislisten].ID AND [KHKPreislisten].[Aktionsliste] = 0))
  order by tl_persnr;