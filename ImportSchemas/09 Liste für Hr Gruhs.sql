select  art.artnr,
			art.bez1,
			alutec_art.bezeichnung1,
			(if art.bez1 <> alutec_art.bezeichnung1 then 'X' else '' endif) bez1_diff,
			art.bez2,
			alutec_art.bezeichnung2,
			(if art.bez2 <> alutec_art.bezeichnung2 then 'X' else '' endif) bez2_diff,
			art.suchwort,
			alutec_art.matchcode,
			(if lower(art.suchwort) <> lower(alutec_art.matchcode) then 'X' else '' endif) suchwort_diff,
			(art.sperrkz) geloescht
from alutec_art , art
where (length(alutec_art.bezeichnung1) > 40 or length(alutec_art.bezeichnung2) > 40 or length(alutec_art.matchcode) > 40)
AND art.artnr = alutec_art.artnr; 