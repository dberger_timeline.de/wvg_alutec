Update bel, laender
Set bel.adr1land = laender.name, bel.laender_kz = laender.nr
Where bel.adr1land = laender.iso2;

--belpos.status korrekt setzen
update belpos set lager_nr = 1 where bel_typ in (2,6) ;
update bel set ind_long_01 = 0 where ind_long_01 is null;
update belpos set ind_long_01 = 0 where ind_long_01 is null;
 
update "dueb_khk_vkvorgaengspos",   
         "belpos",   
         "bel"  
 
set belpos.uebernommen = dueb_khk_vkvorgaengspos.geliefert,
belpos.status = if dueb_khk_vkvorgaengspos.erfuellt <> -1 then 0 else 99 endif
 
   WHERE ( "bel"."typ" = "belpos"."bel_typ" ) and  
         ( "bel"."nr" = "belpos"."bel_nr" ) and
"bel"."ind_long_01" > 0 AND  
"belpos"."ind_long_01" > 0 AND  
// "dueb_khk_vkvorgaengspos"."vorid" = 15063 and 
// "dueb_khk_vkvorgaengspos"."vorposid" = 52536 and
         ( "dueb_khk_vkvorgaengspos"."vorid" = "bel"."ind_long_01" ) and  
         ( "dueb_khk_vkvorgaengspos"."vorposid" = "belpos"."ind_long_01" )  
and bel.typ = 2 
order by bel_typ, bel_nr, posnr ;
 
update bel
set status = 'T'
where typ = 2
and exists (select 1 from belpos where bel_typ= bel.typ and bel_nr = bel.nr and belpos.status < 99 and pos_typ = 'A');

update bel key join belpos, dueb_khk_vkvorgaenge 
set belpos.status = 99, bel.status = 'K'
where bel.typ = 2 
and bel.ind_long_01 = dueb_khk_vkvorgaenge .vorid
and dueb_khk_vkvorgaenge.erfuellt = -1;

update bel, laender 
set bel.mwst_kz = 3, bel.mwst_tab_nr = 3
where bel.laender_kz = LAENDER.nr
and LAENDER.eu = 0
order by bel.nr desc;

update bel, laender 
set bel.mwst_kz = 2, bel.mwst_tab_nr = 2
where bel.laender_kz = LAENDER.nr
and LAENDER.nr <> 46
and LAENDER.eu = 1
order by bel.nr desc;

update bel, laender 
set bel.mwst_kz = 1, bel.mwst_tab_nr = (select first nr from mwst where mwst_kz = 1 and date(mwst.gueltig_ab) <= date(bel.datum) order by mwst.gueltig_ab desc)
where bel.laender_kz = LAENDER.nr
and LAENDER.nr = 46
and LAENDER.eu = 1
order by bel.nr desc;


update "dueb_khk_ekvorgaengspos",    
         "belpos",    
         "bel"   
set belpos.uebernommen = dueb_khk_ekvorgaengspos.geliefert, 
belpos.status = if dueb_khk_ekvorgaengspos.erfuellt <> -1 then 0 else 99 endif 
   WHERE ( "bel"."typ" = "belpos"."bel_typ" ) and   
         ( "bel"."nr" = "belpos"."bel_nr" ) and 
"bel"."ind_long_01" > 0 AND   
"belpos"."ind_long_01" > 0 AND   
         ( "dueb_khk_ekvorgaengspos"."vorid" = "bel"."ind_long_01" ) and   
         ( "dueb_khk_ekvorgaengspos"."vorposid" = "belpos"."ind_long_01" )   
and bel.typ = 6
order by bel_typ, bel_nr, posnr ; 
 
update bel 
set status = 'T' 
where typ = 6
and exists (select 1 from belpos where bel_typ= bel.typ and bel_nr = bel.nr and belpos.status < 99 and pos_typ = 'A'); 
 
update bel key join belpos, dueb_khk_ekvorgaenge  
set belpos.status = 99, bel.status = 'K' 
where bel.typ = 6 
and bel.ind_long_01 = dueb_khk_ekvorgaenge .vorid 
and dueb_khk_ekvorgaenge.erfuellt = -1; 
 