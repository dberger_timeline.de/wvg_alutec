﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.TypedDataSets;

using TimeLine.Modules.Info;

namespace WVG_CustomInfo
{
    public class busInfoItemCustom : busInfoItem
    {
        public override void Retrieve()
        {
            base.Retrieve();
        }

        public override void Save()
        {
            base.Save();
        }
    }
}
