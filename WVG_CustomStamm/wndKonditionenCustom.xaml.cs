﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

using TimeLine.Client.Controls.DX.Docking;
using TimeLine.Client.Controls.DX.Grid;

using TimeLine.Client.Controls.DX.Chart;
using TimeLine.Client.Controls.DX.Gauge;
using TimeLine.Client.Controls.DX.Pivot;

using TimeLine.Client.Controls.TX.RTF;

using TimeLine.Modules.Konditionen;
using TimeLine.TypedDataSets;

namespace WVG_CustomStamm
{
    public partial class wndKonditionenCustom : wndKonditionen
    {

		public new busKonditionenCustom BusObj
		{
			get
			{
				return base.BusObj as busKonditionenCustom;
			}
		}


		#region override WHO methods

		/// <summary>
        /// Place to do module's and controls' initializations. 
		/// At this point, all controls are created.
		/// </summary>
        public override void Opened()
        {
            base.Opened();
        }

		public override void ItemChanged(UIElement container, FrameworkElement element, object selectedItem, object newValue, object oldValue)
        {
            base.ItemChanged(container, element, selectedItem, newValue, oldValue);

            ////  Here you will code the actions for the individual controls
            //if (element == controlName)
            //{
			//	
            //}
        }
	    
		public override void ButtonClicked(xButton sourceControl)
        {
            base.ButtonClicked(sourceControl);

            ////  Here you will code the actions for the individual controls
            //if (sourceControl == controlName)
            //{
			//	
            //}
        }

        protected override void OnRetrieve(params object[] args)
        {
             base.OnRetrieve(args);

            //// base class code to override here:
            //if (args == null || args.Length == 0)  // called from miRetrieve
            //{
            //    if (BusObj.dSet.ArgTypes.Count > 0) 
            //    {
            //        // display "Input Args" window
            //        var winArgs = new xDSetArgsWindow(BusObj.dSet);
            //        winArgs.ShowDialog();
            //        if (winArgs.Cancelled)
            //        {
            //            Log.Error("Retrieve abgebrochen.");
            //            return;
            //        }
            //    }
            //    BusObj.dSet.Retrieve();
            //}
        }

        public override int OnSave()
        {
            return base.OnSave();
        }
        
        private void conditionsalutec_gesperrt_jn_ItemChanged(object e, TimeLine.Client.Framework.NonVisual.RoutedEvents.RoutedEventArgs<object, object> s)
        {
            //DebuggerUtils.Launch();
        	if ( conditionsalutec_gesperrt_jn.Value.ToInt(0) == 1 )
        	    {
                    //var persartRow = BusObj.tSet.conditions.FirstOrDefault();
                    //persartRow.alutec_gesperrt_am = DateTime.Today;
        			dgConditions.SelectedRow.alutec_gesperrt_am = DateTime.Today;
        	    }
        	else
        	    {
        	    	//var persartRow = BusObj.tSet.conditions.FirstOrDefault();
                    //persartRow.alutec_gesperrt_am = null;
                    dgConditions.SelectedRow.alutec_gesperrt_am = null;
        	    }
        }
        
        private void conditionsalutec_gesperrt_jn1_ItemChanged(object e, TimeLine.Client.Framework.NonVisual.RoutedEvents.RoutedEventArgs<object, object> s)
        {
        	dsKonditionen.conditionsRow row;
        	
            //DebuggerUtils.Launch();
            if ( rbgZuordTyp.Value.ToInt(0) == 0 )
            {
            	//EK
            	if ( conditionsalutec_gesperrt_jn1.Value.ToInt(0) == 1 )
            	{
            		row = dgItemPersArtEK.SelectedRow;
            		row.alutec_gesperrt_am = DateTime.Today;
            	}
            	else
            	{
            		row = dgItemPersArtEK.SelectedRow;
            		row.alutec_gesperrt_am = null;
            	}
            }
            if ( rbgZuordTyp.Value.ToInt(0) == 1 )
            {
            	//VK
            	if ( conditionsalutec_gesperrt_jn1.Value.ToInt(0) == 1 )
            	{
            		row = dgItemPersArtVK.SelectedRow;
            		row.alutec_gesperrt_am = DateTime.Today;
            	}
            	else
            	{
            		row = dgItemPersArtVK.SelectedRow;
            		row.alutec_gesperrt_am = null;
            	}
            }
            if ( rbgZuordTyp.Value.ToInt(0) == 2 )
            {
            	//XTF
            	if ( conditionsalutec_gesperrt_jn1.Value.ToInt(0) == 1 )
            	{
            		row = dgItemPersArtBearb.SelectedRow;
            		row.alutec_gesperrt_am = DateTime.Today;
            	}
            	else
            	{
            		row = dgItemPersArtBearb.SelectedRow;
            		row.alutec_gesperrt_am = null;
            	}
            }

            	
        	if ( conditionsalutec_gesperrt_jn1.Value.ToInt(0) == 1 )
        	    {
        			var persartRow = BusObj.tSet.conditions.FirstOrDefault();
        			persartRow.alutec_gesperrt_am = DateTime.Today;
        	    }
        	else
        	    {
        	    	var persartRow = BusObj.tSet.conditions.FirstOrDefault();
        			persartRow.alutec_gesperrt_am = null;
        	    }
        }
        #endregion
    }
}
