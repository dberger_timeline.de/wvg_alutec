﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Threading.Tasks;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

using TimeLine.Client.Controls.DX.Docking;
using TimeLine.Client.Controls.DX.Grid;

using TimeLine.Client.Controls.DX.Chart;
using TimeLine.Client.Controls.DX.Gauge;
using TimeLine.Client.Controls.DX.Pivot;

using TimeLine.Client.Controls.TX.RTF;

using TimeLine.Modules.BusinessPartner;

namespace WVG_CustomStamm
{
    public partial class wndBusinessPartnerCustom : wndBusinessPartner
    {

		public new busBusinessPartnerCustom BusObj
		{
			get
			{
				return base.BusObj as busBusinessPartnerCustom;
			}
		}
		
		public override Task OnOpened()
		{
			
			persText.MouseDoubleClick += persText_MouseDoubleClick;
			return base.OnOpened();
		}

		public override Task OnClosed()
		{
			persText.MouseDoubleClick -= persText_MouseDoubleClick;
			
			return base.OnClosed();
			
		}

        private void persText_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var response = new TimeLine.AppContent.WHO.WVGLangtextEdit.Input { inText = BusObj.PersRow.text };
            var output = TimeLine.AppContent.Module.WVGLangtextEdit.OpenResponse(response);
            if ( output != null )
            {
                if ( !output.isCancelled )
                {
                    if ( output.outText != null)
                    {
                    	BusObj.PersRow.text = output.outText.ToStringNN();
                    }
                }
            }
            return;
        }
    }
}
