﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Data.Exceptions;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;
using TimeLine.Framework.Util.Exceptions;
using TimeLine.Framework.Security;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

namespace WVG_CustomStamm
{
    public partial class wndWVGLangtextEdit : TimeLine.Client.Framework.xWindowHandlingObjectGeneric
    {
    	
		public virtual string mitarbKuerzel
		{
			get
			{
	            if (UserSecurityToken.Instance != null)
	            {
                    //var mitarbRow = this.DataCache.GetCurrentUserMitarb(UserSecurityToken.Instance.UserID);
                    //if (mitarbRow != null)
                    //{
                    //    return mitarbRow["kuerzel"] as string;
                    //}
                    return UserSecurityToken.Instance.UserID;
	            }
	            return "";
			}
		}
		
    	/// <summary>
    	/// Override this method to code actions after the Opened event terminates and the current WindowContent is loaded.
    	/// </summary>
    	public override void PostOpen()
    	{
    		base.PostOpen();
    		
    		tbText.Text = RetrievalArguments.inText;
    		
    	}

		public override void ButtonClicked(xButton sourceControl)
        {
			
            base.ButtonClicked(sourceControl);
			
			switch (sourceControl.Name)
            {
                case "bOk":
                    ReturnValue = new TimeLine.AppContent.WHO.WVGLangtextEdit.Output { outText= tbText.Text, isCancelled = false };
                    Close();
                    break;
                case "bCancel":  
                    ReturnValue = new TimeLine.AppContent.WHO.WVGLangtextEdit.Output { isCancelled = true };
                    Close();
                    break;
				case "bTimeStmp":
					var text = tbText.Text;
					text = text + Environment.NewLine + Environment.NewLine + "[" + DateTime.Now.ToString() + " " + mitarbKuerzel + "] ";
					tbText.Text = text;
					
					tbText.Focus();
					tbText.CaretIndex = tbText.Text.Length;
					break;
                default:
                    break;
            }
        }
    }
}
