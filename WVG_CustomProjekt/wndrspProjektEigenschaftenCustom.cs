﻿// THIS CODE HAS BEEN GENERATED BY THE XamlGenerator TOOL AND WILL BE OVERWRITTEN. DO NOT CHANGE OR REMOVE !
#region AUTOGENERATED CODE

using TimeLine.Framework;

namespace WVG_CustomProjekt
{
	/// <summary>
    /// The wndrspProjektEigenschaftenCustom xWindowhandlingObject
    /// </summary>
    public partial class wndrspProjektEigenschaftenCustom
    {
		#region Properties



		#endregion

        #region Controls

		/// <summary>
		/// Gets the mainTabEing TimeLine.Client.Controls.DX.Docking.xDockTab
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Docking.xDockTab mainTabEing { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Docking.xDockTab>("mainTabEing"); } }

		/// <summary>
		/// Gets the dtabProjektangaben TimeLine.Client.Controls.DX.Docking.xDockTabItem
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Docking.xDockTabItem dtabProjektangaben { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Docking.xDockTabItem>("dtabProjektangaben"); } }

		/// <summary>
		/// Gets the xgProjektangaben TimeLine.Client.Controls.xGrid
		/// </summary>
		private global::TimeLine.Client.Controls.xGrid xgProjektangaben { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xGrid>("xgProjektangaben"); } }

		/// <summary>
		/// Gets the dpprojekte TimeLine.Client.Controls.xDataPanel
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanel dpprojekte { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanel>("dpprojekte"); } }

		/// <summary>
		/// Gets the rbgProjektTyp TimeLine.Client.Controls.xDataPanelRadioButtonGroupItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelRadioButtonGroupItem rbgProjektTyp { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelRadioButtonGroupItem>("rbgProjektTyp"); } }

		/// <summary>
		/// Gets the lbTypTemplate System.Windows.Controls.ListBoxItem
		/// </summary>
		private global::System.Windows.Controls.ListBoxItem lbTypTemplate { get { return GetControlByNameAndTyp<global::System.Windows.Controls.ListBoxItem>("lbTypTemplate"); } }

		/// <summary>
		/// Gets the bTemplate TimeLine.Client.Controls.xButton
		/// </summary>
		private global::TimeLine.Client.Controls.xButton bTemplate { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xButton>("bTemplate"); } }

		/// <summary>
		/// Gets the tbsProjektId TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbsProjektId { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbsProjektId"); } }

		/// <summary>
		/// Gets the tbsuchwort TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbsuchwort { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbsuchwort"); } }

		/// <summary>
		/// Gets the tbkommentar TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbkommentar { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbkommentar"); } }

		/// <summary>
		/// Gets the xtbsPers TimeLine.Client.Controls.xDataPanelTextBoxSearchItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxSearchItem xtbsPers { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxSearchItem>("xtbsPers"); } }

		/// <summary>
		/// Gets the tbpers_suchwort TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbpers_suchwort { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbpers_suchwort"); } }

		/// <summary>
		/// Gets the tbpers_name1 TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbpers_name1 { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbpers_name1"); } }

		/// <summary>
		/// Gets the cb_fibu_cobj_typ TimeLine.Client.Controls.xDataPanelComboBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelComboBoxItem cb_fibu_cobj_typ { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelComboBoxItem>("cb_fibu_cobj_typ"); } }

		/// <summary>
		/// Gets the tbs_fibu_cobj_nr TimeLine.Client.Controls.xDataPanelTextBoxSearchItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxSearchItem tbs_fibu_cobj_nr { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxSearchItem>("tbs_fibu_cobj_nr"); } }

		/// <summary>
		/// Gets the lg_lager_nr TimeLine.Client.Controls.xDataPanelLookupItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelLookupItem lg_lager_nr { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelLookupItem>("lg_lager_nr"); } }

		/// <summary>
		/// Gets the lg_status TimeLine.Client.Controls.xDataPanelLookupItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelLookupItem lg_status { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelLookupItem>("lg_status"); } }

		/// <summary>
		/// Gets the ck_projekte_erledigt TimeLine.Client.Controls.xDataPanelCheckBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelCheckBoxItem ck_projekte_erledigt { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelCheckBoxItem>("ck_projekte_erledigt"); } }

		/// <summary>
		/// Gets the tbtemplate TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbtemplate { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbtemplate"); } }

		/// <summary>
		/// Gets the dp TimeLine.Client.Controls.xDataPanel
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanel dp { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanel>("dp"); } }

		/// <summary>
		/// Gets the lg_projekte_projekt_leiter TimeLine.Client.Controls.xDataPanelLookupItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelLookupItem lg_projekte_projekt_leiter { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelLookupItem>("lg_projekte_projekt_leiter"); } }

		/// <summary>
		/// Gets the xlg_pers_tel_lfd_nr TimeLine.Client.Controls.xDataPanelLookupItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelLookupItem xlg_pers_tel_lfd_nr { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelLookupItem>("xlg_pers_tel_lfd_nr"); } }

		/// <summary>
		/// Gets the tbpers_tel_name TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbpers_tel_name { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbpers_tel_name"); } }

		/// <summary>
		/// Gets the tbpers_tel_funktions_schl TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbpers_tel_funktions_schl { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbpers_tel_funktions_schl"); } }

		/// <summary>
		/// Gets the tbprojekte_pers_tel_adr_nr TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbprojekte_pers_tel_adr_nr { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbprojekte_pers_tel_adr_nr"); } }

		/// <summary>
		/// Gets the tbpers_tel_tel TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbpers_tel_tel { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbpers_tel_tel"); } }

		/// <summary>
		/// Gets the tbpers_tel_fax TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbpers_tel_fax { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbpers_tel_fax"); } }

		/// <summary>
		/// Gets the tbpers_tel_email TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbpers_tel_email { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbpers_tel_email"); } }

		/// <summary>
		/// Gets the tbpers_tel_handy TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbpers_tel_handy { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbpers_tel_handy"); } }

		/// <summary>
		/// Gets the tbpers_tel_kommentar TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbpers_tel_kommentar { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbpers_tel_kommentar"); } }

		/// <summary>
		/// Gets the xtbsbAdr TimeLine.Client.Controls.xDataPanelTextBoxSearchItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxSearchItem xtbsbAdr { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxSearchItem>("xtbsbAdr"); } }

		/// <summary>
		/// Gets the tbcf_adrname1 TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbcf_adrname1 { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbcf_adrname1"); } }

		/// <summary>
		/// Gets the tbcf_adrzeile1 TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbcf_adrzeile1 { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbcf_adrzeile1"); } }

		/// <summary>
		/// Gets the gc TimeLine.Client.Controls.xGroupControl
		/// </summary>
		private global::TimeLine.Client.Controls.xGroupControl gc { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xGroupControl>("gc"); } }

		/// <summary>
		/// Gets the tbcf_adrplz TimeLine.Client.Controls.xTextBox
		/// </summary>
		private global::TimeLine.Client.Controls.xTextBox tbcf_adrplz { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xTextBox>("tbcf_adrplz"); } }

		/// <summary>
		/// Gets the tbcf_adrort TimeLine.Client.Controls.xTextBox
		/// </summary>
		private global::TimeLine.Client.Controls.xTextBox tbcf_adrort { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xTextBox>("tbcf_adrort"); } }

		/// <summary>
		/// Gets the tbcf_adrland TimeLine.Client.Controls.xDataPanelTextBoxItem
		/// </summary>
		private global::TimeLine.Client.Controls.xDataPanelTextBoxItem tbcf_adrland { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xDataPanelTextBoxItem>("tbcf_adrland"); } }

		/// <summary>
		/// Gets the dtabMeilensteine TimeLine.Client.Controls.DX.Docking.xDockTabItem
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Docking.xDockTabItem dtabMeilensteine { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Docking.xDockTabItem>("dtabMeilensteine"); } }

		/// <summary>
		/// Gets the dxgprojekt_meilensteine TimeLine.Client.Controls.DX.Grid.DxDataGrid
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxDataGrid dxgprojekt_meilensteine { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxDataGrid>("dxgprojekt_meilensteine"); } }

		/// <summary>
		/// Gets the tbtyp TimeLine.Client.Controls.DX.Grid.DxComboBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxComboBoxColumn tbtyp { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxComboBoxColumn>("tbtyp"); } }

		/// <summary>
		/// Gets the tbid TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbid { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbid"); } }

		/// <summary>
		/// Gets the tbbez TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbbez { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbbez"); } }

		/// <summary>
		/// Gets the tbende TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbende { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbende"); } }

		/// <summary>
		/// Gets the tbdauer TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbdauer { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbdauer"); } }

		/// <summary>
		/// Gets the tbfarbe TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbfarbe { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbfarbe"); } }

		/// <summary>
		/// Gets the tbeinkaufsbudget TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbeinkaufsbudget { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbeinkaufsbudget"); } }

		/// <summary>
		/// Gets the tbprojektierte_summe TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbprojektierte_summe { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbprojektierte_summe"); } }

		/// <summary>
		/// Gets the tbprojektierte_std TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbprojektierte_std { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbprojektierte_std"); } }

		/// <summary>
		/// Gets the tbrealisierungsgrad TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbrealisierungsgrad { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbrealisierungsgrad"); } }

		/// <summary>
		/// Gets the tbmitarb_id TimeLine.Client.Controls.DX.Grid.DxLookupColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxLookupColumn tbmitarb_id { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxLookupColumn>("tbmitarb_id"); } }

		/// <summary>
		/// Gets the tbabteilg TimeLine.Client.Controls.DX.Grid.DxLookupColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxLookupColumn tbabteilg { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxLookupColumn>("tbabteilg"); } }

		/// <summary>
		/// Gets the tbintern TimeLine.Client.Controls.DX.Grid.DxCheckBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxCheckBoxColumn tbintern { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxCheckBoxColumn>("tbintern"); } }

		/// <summary>
		/// Gets the tbbez_fremdsprachlich TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbbez_fremdsprachlich { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbbez_fremdsprachlich"); } }

		/// <summary>
		/// Gets the tbbemerkung TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbbemerkung { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbbemerkung"); } }

		/// <summary>
		/// Gets the tbsicherheitszeit TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn tbsicherheitszeit { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxTextBoxColumn>("tbsicherheitszeit"); } }

		/// <summary>
		/// Gets the tbstatus TimeLine.Client.Controls.DX.Grid.DxLookupColumn
		/// </summary>
		private global::TimeLine.Client.Controls.DX.Grid.DxLookupColumn tbstatus { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.DX.Grid.DxLookupColumn>("tbstatus"); } }

		/// <summary>
		/// Gets the bOk TimeLine.Client.Controls.xButton
		/// </summary>
		private global::TimeLine.Client.Controls.xButton bOk { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xButton>("bOk"); } }

		/// <summary>
		/// Gets the bCancel TimeLine.Client.Controls.xButton
		/// </summary>
		private global::TimeLine.Client.Controls.xButton bCancel { get { return GetControlByNameAndTyp<global::TimeLine.Client.Controls.xButton>("bCancel"); } }


        #endregion

		#region Events

		public override void RegisterDesignerEvents()
		{

		}

		public override void UnregisterDesignerEvents()
		{

		}

		#endregion
    }
}
#endregion
