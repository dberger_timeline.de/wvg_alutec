﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

using TimeLine.Client.Controls.DX.Docking;
using TimeLine.Client.Controls.DX.Grid;

using TimeLine.Client.Controls.DX.Chart;
using TimeLine.Client.Controls.DX.Gauge;
using TimeLine.Client.Controls.DX.Pivot;

using TimeLine.Client.Controls.TX.RTF;

using TimeLine.Modules.ProjektPainter;
using TimeLine.TypedDataSets;

namespace WVG_CustomProjekt
{
    public partial class wndrspProjektEigenschaftenCustom : wndProjektEigenschaften
    {

		public new busProjektEigenschaftenCustom BusObj
		{
			get
			{
				return base.BusObj as busProjektEigenschaftenCustom;
			}
		}


		#region override WHO methods
		
		public override dsProjektEigenschaften.projekteRow NewProjekt()
		{
            IsProjektNew = true;
            tbsProjektId.IsEnabled = true;
            rbgProjektTyp.IsEnabled = true;
            bTemplate.Visibility = Visibility.Visible;

            Module.Title = l.Translate("Neues Projekt");

            var projektTyp = (Enums.ProjekteTyp)rbgProjektTyp.Value.ToInt(Enums.ProjekteTyp.Projekt);
            var row = BusObj.NewProjekt(projektTyp);
			
            //alutec ProjektID wird automatisch vergeben - also aus dem BusObj nehmen...
            ProjektId = row.id;
            //alutec Ende
            
            tbsProjektId.SetFocus();
            
            return row;
		}

		#endregion
    }
}
