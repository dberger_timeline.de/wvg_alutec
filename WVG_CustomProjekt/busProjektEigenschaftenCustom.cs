﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.TypedDataSets;

using TimeLine.BusinessObjects;

namespace WVG_CustomProjekt
{
    public class busProjektEigenschaftenCustom : busProjektEigenschaften
    {
        public override void Retrieve()
        {
            base.Retrieve();
        }

        public override void Save()
        {
            base.Save();
        }
        
        public override dsProjektEigenschaften.projekteRow NewProjekt(Enums.ProjekteTyp? typ)
        {
            var row = tSet.projekte.CreateRow();
            row.typ = typ;
            row.datum = DateTime.Now;
            
            //alutec ProjektNr vergeben
            //DebuggerUtils.Launch();
            var ltztNr = Sql.SelectValue("select first RIGHT(id,4) from projekte where id like 'P' + RIGHT(CAST(year(today()) AS CHAR),2) + '-____' and isnumeric(RIGHT(id,4)) = 1 order by id desc").ToInt(0);
            ltztNr = ltztNr + 1;
            
            string neueNr = "0000" + ltztNr.ToString();
            neueNr = "P" + DateTime.Today.Year.ToString().Right(2) + "-" + neueNr.Right(4);
            row.id = neueNr;
            //alutec Ende
            
            return row;
        }

    }
}
