﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Data.Exceptions;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;
using TimeLine.Framework.Util.Exceptions;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

namespace WVG_Warengruppen
{
    public partial class wndAlutecWarenGrp : TimeLine.Client.Framework.xWindowHandlingObjectGeneric<WVG_Warengruppen.busAlutecWarenGrp>
    {

        #region Constructor
 	    /// <summary>
 	    /// Constructor with busObjId initialisation;
		/// Caution: controls are not created yet - use the Opened method for their intialisation
 	    /// </summary>
        public wndAlutecWarenGrp() : base("busAlutecWarenGrp")
        {
        }
		#endregion


		#region override WHO methods
		/// <summary>
        /// Place to do module's and controls' initializations. 
		/// At this point, all controls are created.
		/// </summary>
        public override void Opened()
        {
            base.Opened();
        }

        /// <summary>
        /// Override this method to code actions after the Opened event terminates and the current WindowContent is loaded.
        /// </summary>
        public override void PostOpen()
        {
        	base.PostOpen();
        	
        	BusObj.Retrieve();
        	
        }

		public override void ItemChanged(UIElement container, FrameworkElement element, object selectedItem, object newValue, object oldValue)
        {
            base.ItemChanged(container, element, selectedItem, newValue, oldValue);

            ////  Here you will code the actions for the individual controls
            //if (element == controlName)
            //{
			//	
            //}
        }
	    
		public override void ButtonClicked(xButton sourceControl)
        {
            base.ButtonClicked(sourceControl);

            ////  Here you will code the actions for the individual controls
            //if (sourceControl == controlName)
            //{
			//	
            //}
        }

        public override void MenuItemClicked(xMenuItem sourceControl)
        {
			base.MenuItemClicked(sourceControl);

			 ////  Here you will code the actions for the individual controls
            //if (sourceControl == controlName)
            //{
			//	
            //}
        }

        protected override void OnRetrieve(params object[] args)
        {
             base.OnRetrieve(args);

            //// base class code to override here:
            //if (args == null || args.Length == 0)  // called from miRetrieve
            //{
            //    if (BusObj.dSet.ArgTypes.Count > 0) 
            //    {
            //        // display "Input Args" window
            //        var winArgs = new xDSetArgsWindow(BusObj.dSet);
            //        winArgs.ShowDialog();
            //        if (winArgs.Cancelled)
            //        {
            //            Log.Error("Retrieve abgebrochen.");
            //            return;
            //        }
            //    }
            //    BusObj.dSet.Retrieve();
            //}
        }

        public override int OnSave()
        {
            return base.OnSave();
        }
		#endregion
    }
}
