﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Data.Exceptions;
using TimeLine.Framework;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Exceptions;
using TimeLine.Framework.Util.Logging;
using TimeLine.TypedDataSets;

namespace WVG_Warengruppen
{
    public class busAlutecWarenGrp : xObject
    {
        public dsAlutecWarenGrp tSet { get; set; }

        protected busAlutecWarenGrp()
        {
            tSet = new dsAlutecWarenGrp();
            dSet = tSet.DataSet;
        }
    }
}
