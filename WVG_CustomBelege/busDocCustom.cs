﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.TypedDataSets;

using TimeLine.BusinessObjects;
using TimeLine.BusinessObjects.Utils;

namespace WVG_CustomBelege
{
    public class busDocCustom : busDoc
    {
        public override void Retrieve()
        {
        	base.Retrieve();
        }

		#region Save, Anpassung alu-tec: Frachtberechnung
        public override void Save()
        {
        	//Fracht in Verkaufsbelegen mit 3% des Nettowerts vorbelegen - erstmal deaktiviert 2021-10-25
                //if (BelRow.typ.ToInt() == 1 || BelRow.typ.ToInt() == 2)
                //{ BelRow.fracht_in_whrg = BelRow.summenetto / 100 * 3; }                    
            base.Save();
        }
        #endregion
        
		#region RetrieveArt, Anpassung alu-tec: Prüfung ob Kondition gesperrt
        public override dsBelposArt RetrieveBelposArt(string artNr)
        {
        	var belegRow = BelRow;
        	var persTyp = belegRow.pers_typ.Value.ToInt();
        	var persNr = belegRow.pers_nr.Value.ToInt();
        	var result = Sql.SelectValue("SELECT alutec_gesperrt_jn FROM persart WHERE pers_typ = :typ AND pers_nr = :nr AND art_artnr = :artnr AND typ = 0", new { typ = persTyp, nr = persNr, artnr = artNr }).ToInt(0);
        	
        	if (result == 1)
        	{
        		BusGlobals.ShowError("Fehler!", "Der Artikel kann nicht geladen werden, da diese Kondition als >gesperrt< markiert wurde.", Severity.Exception, false);
        		return null;
        	}
        	
        	return base.RetrieveBelposArt(artNr);        	
        }

        /// <summary>
        /// Loads and calculate in belpos some details about a certain Item.
        /// Returns:
        /// 1 - success,
        /// -1 - failure
        /// </summary>
        /// <param name="belposRowNr">The belpos row index</param>
        /// <param name="artNr">The artnr of the target item</param>
        public override void RetrieveArt(dsBel.belposRow belposRow, string artNr, bool quiet, bool recalcBelposPrice)
        {
        	base.RetrieveArt(belposRow, artNr, quiet, recalcBelposPrice);
        	
        	var belegRow = BelRow;
        	var persTyp = belegRow.pers_typ.Value.ToInt();
        	var persNr = belegRow.pers_nr.Value.ToInt();
        	var kommentar = Sql.SelectValue("SELECT kommentar FROM persart WHERE pers_typ = :typ AND pers_nr = :nr AND art_artnr = :artnr AND typ = 0", new { typ = persTyp, nr = persNr, artnr = artNr }).ToStringNN();
        	if ( kommentar != "" )
        	{
        		belposRow.text = belposRow.text + System.Environment.NewLine + System.Environment.NewLine + kommentar;
        	}

        }
        
        
        /// <summary>
        /// refresh the calculation on ZSL rows fields according to berech_art setting
        /// </summary>
        /// <param name="rowBelposDetZSL"></param>
        /// <param name="belposHeaderRow"></param>
        public override void CalcZSLFields(dsBel.belpos_detailRow rowBelposDetZSL, dsBel.belposRow belposHeaderRow)
        {
			if (rowBelposDetZSL.zsl_zusatz_fkt.ToInt(0) == (int)EnumZSLZusatzfkt.MTZ)
			{
				if (rowBelposDetZSL.mtz_nr == null)
				{
					rowBelposDetZSL.zendpreis = 0;
				}
				else
				{
					dsBel.mtzRow mtzRow = tSet.mtz.Where(x => x.nr == rowBelposDetZSL.mtz_nr.ToInt(0)).FirstOrDefault();
					rowBelposDetZSL.zendpreis = GetMtzZslPreis(belposHeaderRow, rowBelposDetZSL, mtzRow?.mtz_konditionen_row == null ? rowBelposDetZSL.mtz_nr : mtzRow.mtz_konditionen_row, rowBelposDetZSL.zslid);
				}

				CalculateZSLSums(belposHeaderRow);
				return;
			}

			if (rowBelposDetZSL.berech_art == Enums.BerechArt.Prozentual)
			{
				rowBelposDetZSL.zendpreis_abs = 0;
				rowBelposDetZSL.zendpreis = belposHeaderRow.einzelpr * (rowBelposDetZSL.menge / 100);
				rowBelposDetZSL.zsl_preis_ges = rowBelposDetZSL.zendpreis * belposHeaderRow.menge;
			}
			else if (rowBelposDetZSL.berech_art == Enums.BerechArt.Absolut)
			{
				rowBelposDetZSL.zendpreis_abs = rowBelposDetZSL.menge;
				rowBelposDetZSL.zendpreis = 0;
				rowBelposDetZSL.zsl_preis_ges = rowBelposDetZSL.menge;
			}
			else if (rowBelposDetZSL.berech_art == Enums.BerechArt.AbsolutProMengeneinheit)
			{
				rowBelposDetZSL.zendpreis_abs = 0;
				rowBelposDetZSL.zendpreis = rowBelposDetZSL.menge;
				rowBelposDetZSL.zsl_preis_ges = rowBelposDetZSL.menge * belposHeaderRow.menge;
			}
			else if (rowBelposDetZSL.berech_art == Enums.BerechArt.ProKilogrammBelepositionGewicht)
			{
				// because ZSL is expressed pro KG, as default weight, take the faktor corresponding to the belpos position gewicht,
				// to calculate ZSL to the equivalent belpos menge * gewicht into default weight (Kg) via factor
				double gew_faktor = belposHeaderRow.me_faktor_klassenr.ToDouble(1);
				double gewicht = belposHeaderRow.posgewicht ?? 0;// belposHeaderRow.gewicht ?? 0;

				rowBelposDetZSL.zendpreis_abs = rowBelposDetZSL.menge * gew_faktor * gewicht;// 0;
				rowBelposDetZSL.zendpreis = 0;// rowBelposDetZSL.menge * gew_faktor * gewicht;
				rowBelposDetZSL.zsl_preis_ges = rowBelposDetZSL.menge * gew_faktor * gewicht;
			}
			//alu-tec Bugfix! Start
			else if (rowBelposDetZSL.berech_art == Enums.BerechArt.ProKilogrammGussMTZGewicht && rowBelposDetZSL.zsl_zusatz_fkt.ToInt(0) != (int)EnumZSLZusatzfkt.MTZ)
			{
				double gew_faktor = belposHeaderRow.me_faktor_klassenr.ToDouble(1);
				double gewicht = rowBelposDetZSL.gewicht.ToDoubleNN() * belposHeaderRow.menge.ToDoubleNN();

				rowBelposDetZSL.zendpreis_abs = rowBelposDetZSL.menge * gew_faktor * gewicht;// 0;
				rowBelposDetZSL.zendpreis = 0;// rowBelposDetZSL.menge * gew_faktor * gewicht;
				rowBelposDetZSL.zsl_preis_ges = rowBelposDetZSL.menge * gew_faktor * gewicht;
				
			}
			//alu-tec Ende
			
			CalculateZSLSums(belposHeaderRow);
        }

        #endregion
		
        /// <summary>
        /// creates a copy of this bel, for the specified pers_typ, pers_nr
        /// </summary>
        /// <returns>bel.nr of the newly created bel; -1 in case of failure</returns>
        public override int CopyBel(int copyBelNr, int pers_nr, int pers_typ, bool needPersLoad, bool incrementVersion, bool resetMwstAndRecalcBelposPrice)
        {
            //DebuggerUtils.Launch();
            int belTyp = tSet.bel.DefaultViewRows[0].typ;
            int belNr = tSet.bel.DefaultViewRows[0].nr;
            if (belTyp != Enums.BelTyp.Angebot || incrementVersion == false)
            {
            	return base.CopyBel(copyBelNr, pers_nr, pers_typ, needPersLoad, incrementVersion, resetMwstAndRecalcBelposPrice);
            }
        	
            //in der alten Angebotsversion den Ablehnungsgrund setzen
            var newNr = base.CopyBel(copyBelNr, pers_nr, pers_typ, needPersLoad, incrementVersion, resetMwstAndRecalcBelposPrice).ToInt(0);
            if (newNr > 0)
            {
            	
            	Sql.Execute(base.Transaction, "UPDATE bel SET awkz = 99 WHERE typ = 1 AND nr = :nr", new { nr = belNr });
            	Commit();
            }
            
        	return newNr;
        	
        }

    }
}
