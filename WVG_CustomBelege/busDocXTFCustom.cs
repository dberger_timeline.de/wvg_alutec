﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.TypedDataSets;

using TimeLine.BusinessObjects;
using TimeLine.Framework.Data.Exceptions;
using TimeLine.BusinessObjects.Utils;

namespace WVG_CustomBelege
{
    public class busDocXTFCustom : busDocXTF
    {
        public override void Retrieve()
        {
            base.Retrieve();
        }

        public override void Save()
        {
            base.Save();
        }
        
        /// <summary>
        /// set the given WA pos type row after RetrieveArt is called using the already retrieved tdsPBA typed def
        /// the belpos.menge = menge_zu_uebern if it is NOT null, otherwise is calculated with help of its given bab_afo row
        /// </summary>
        /// <param name="newbelposWARow"></param>
        /// <param name="belposWWRow"></param>
        /// <param name="afoRow"></param>
        public override void SetWARowfromBAB(dsBel.belposRow newbelposWARow, dsBel.belposRow belposWWRow, dsBab.bab_afoRow afoRow, double? menge_zu_uebern)
        {

            var persTyp = BelRow.pers_typ.ToInt(0);
            var persNr = BelRow.pers_nr.ToInt(0);
            var artNr = newbelposWARow.art_nr.ToStringNN();
            var arbgkat = afoRow.arbgkat_nr.ToStringNN();
            	
			var result = Sql.SelectValue("SELECT alutec_gesperrt_jn FROM persart WHERE pers_typ = :typ AND pers_nr = :nr AND art_artnr = :artnr AND typ = 10 AND arbgkat_nr = :arbgkatid", new { typ = persTyp, nr = persNr, artnr = artNr, arbgkatid = arbgkat }).ToInt(0);
        	
        	if (result == 1)
        	{
        		BusGlobals.ShowError("Fehler!", "Der Artikel kann nicht geladen werden, da diese Kondition als >gesperrt< markiert wurde.", Severity.Exception, false);
        		throw new ValidationException("Vorgang abgebrochen!");
        	}
        	            
        	base.SetWARowfromBAB(newbelposWARow, belposWWRow, afoRow, menge_zu_uebern);
        }

        

    }
}
